// console.log("Hello World");

// JSON Object
// {
//     "city" : "Quezon City",
//     "province" : "Metro Manila",
//     "country" : "Philippines"
// }

// JSON Array
// [
//     {
//         "city" : "Quezon City",
//         "province" : "Metro Manila",
//         "country" : "Philippines"
//     },
//     {
//         "city" : "Manila City",
//         "province" : "Metro Manila",
//         "country" : "Philippines"
//     }
// ]

// JSON Methods
let batchesArr = [
    {
        batchName: "Batch X"
    },
    {
        batchName: "Batch Y"
    }
]
console.log(`This is the original array:`);
console.log(batchesArr);

// Stringify Method
console.log(`Result from stringify method:`);
let stringBatchesArr = JSON.stringify(batchesArr);
console.log(stringBatchesArr);

let data = JSON.stringify({
    name: "John",
    address: {
        city: "Manila",
        country: "Philippines"
    }
});
console.log(data);

// Stringify Method with Variable
// let firstName = prompt(`What is yout first name?`);
// let lasttName = prompt(`What is yout last name?`);
// let age = prompt(`How old are you?`);
// let address = {
//     city: prompt(`Your city:`),
//     country: prompt(`Your country:`)
// }

// let otherData = JSON.stringify({
//     firstName,
//     lasttName,
//     age,
//     address
// });

// console.log(otherData);

//strigify JSON to JavaScript Object
let batchesJSON = `[{"batchName":"Batch X"},{"batchName":"Batch Y"}]`;
let parseBatchesJSON = JSON.parse(batchesJSON);
console.log(parseBatchesJSON);
console.log(parseBatchesJSON[0]);

let stringifiedObject = `{
    "name":"John", "age":"31", "address":{"city":"Manila","country":"Philippines"
}}`;
let parseStringifiedObject = JSON.parse(stringifiedObject);
console.log(parseStringifiedObject);